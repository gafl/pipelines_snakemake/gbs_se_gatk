# GBS SNPs calling pipeline for Single-end Illumina sequencing
## From raw fastq data to vcf file.
## Pipeline features:
### 1) read preprocessing,
### 2) reads QC, fastq report
### 3) mapping with bwa,
### 4) mapping QC report (multiqc)
### 5) SNPs calling using GATK in parallel mode: reference splitted by sequences: #tasks=#samples X #chrs
      a) GATK using "Best Practices Workflows" https://gatk.broadinstitute.org/hc/en-us/sections/360007226651-Best-Practices-Workflows
      b) Using  hard filtering as outlined in GATK docs
       https://gatkforums.broadinstitute.org/gatk/discussion/2806/howto-apply-hard-filters-to-a-call-set
       https://gatk.broadinstitute.org/hc/en-us/articles/360035890471-Hard-filtering-germline-short-variants


:exclamation: :exclamation: !! For the reference fasta file, he reference definition line must not contain space or special character.


## Snakemake features: fastq from csv file, config, modules, SLURM

### Workflow steps are descibed in the dag_rules.pdf
### Snakemake rules are based on [Snakemake modules](https://forgemia.inra.fr/gafl/snakemake_modules)


### Files description:
### 1) Snakefile
    - Snakefile_para_gatk_SE.smk

### 2) Configuration file in yaml format:, paths, singularity images paths, parameters, GATK filters ....
    - config.yaml

### 3) a sbatch file to run the pipeline: (to be edited)
    - run_snakemake_pipeline_gatk.slurm

### 4) A slurm directive (#core, mem,...) in json format. Can be adjusted if needed
    - cluster.json

### 5) samples file in csv format
    Must contens at least 2 columns for SE reads and 3 for PE reads (tab separator )
    SampleName  fq1     fq2
    SampleName : your sample ID
    fq1: fastq file for a given sample
    fq2: read 2 for paired-end reads
    (sp1   ra_R1.fastq.gz   ra_R2.fastq.gz)

    a sample my have multiple fastq files separated by a ','
    (sp1   ra_R1.fastq.gz,rb_R1.fastq.gz   ra_R2.fastq.gz,rb_R2.fastq.gz)

    - samples.csv


## RUN:

### 1) Edit the config.yaml

### 2) Set your samples in the sample.csv

### 3) Adjust the run_snakemake_pipeline_gatk.slurm file

### 3) Run pipelene in dry run mode first:
`sbatch run_snakemake_pipeline_gatk.slurm`

### 4) uncomment the real run line and run the pipeline:
`sbatch run_snakemake_pipeline_gatk.slurm`


### 5) optionaly run post vcf filtering (example use for GWAS)
    1) filter by read depth
    2) keep only biallelic allels
    3) max missing sample for a site
    4) filter by MAF
    5) keep only polymorph sites


`sbatch run_post_filters_and_stats.sh`

#### Documentation being written (still)


